<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as' => 'inicio', function () {
    return view('principal.index');
}]);

Route::get('/gforce', function () {
    return view('gforce.gforce');
})->name('geforce');

Route::get('/rtx', function () {
    return view('rtx.rtx');
})->name('rtx');
