@extends('principal.plantilla')

@section('contenido')

			<!-- Main -->
            <div id="main-wrapper">
					<div class="container">
						<div id="content">

							<!-- Content -->
								<article>

									<h2 style="color: #76b900;">Geforce Experience</h2>

									<p>Cada vez que se lanza una nueva versión de gaming, NVIDIA colabora muy de cerca con sus desarrolladores 
										para mejorar el rendimiento, corregir errores y, en definitiva, ofrecerte la mejor experiencia de gaming. 
										Las tecnologías como NVIDIA GameWorks™ te ofrecen las herramientas necesarias para optimizar tus juegos y obtener 
										estas mejoras mediante la actualización de los drivers Game Ready. </p>

									<h3 style="color: #ffffff;">Optimiza la configuración de tu juego</h3>

									<div class="row">
										<div class="col-6">
											<p><img src="images/gfe.png" alt="" /></p>
										</div>
										<div class="col-6">
											<p>GeForce Experience elimina los problemas en los juegos de PC estructurando la configuración de gráficos 
												de tu juego. ¿No sabes qué nivel de filtrado en texturas establecer en Overwatch? No te preoccupes. 
												NVIDIA aprovecha el poder del centro de datos en la nube de NVIDIA para probar miles de configuraciones de 
												hardware de PC y encontrar el mejor equilibrio de rendimiento y calidad de imagen.</p>
										</div>
									</div>
									
									<h3 style="color: #ffffff;">Juega a través de Nvidia Shield 4k</h3>
									<p>Transmite tus juegos de PC desde tu habitación a la TV de tu sala con la potencia de una tarjeta gráfica GeForce. 
										Simplemente enlaza tu PC a tu SHIELD 4K utilizando la tecnología NVIDIA GameStream™. </p>


									

								</article>

						</div>
					</div>
				</div>
                
@endsection