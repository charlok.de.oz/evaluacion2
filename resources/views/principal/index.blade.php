@extends('principal.plantilla')

@section('contenido')
			<!-- Banner -->
            <div id="banner-wrapper">
					<div id="banner" class="box container">
						<div class="row">
							<div class="col-7 col-12-medium">
								<h2>RTX. IT’S ON.</h2>
								<p>Exprime tus gráficos utilizando el raytracing de nvidia</p>
							</div>
							<div class="col-5 col-12-medium">
								<ul>
									<li><a href="#" class="button large icon solid fa-arrow-circle-right" style="background-color: #76b900;">Vamos</a></li>
									<li><a href="#" class="button alt large icon solid fa-question-circle" style="background-color: #1e1e1e;">Mas información</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			<!-- Features -->
				<div id="features-wrapper">
					<div class="container">
						<div class="row">
							<div class="col-4 col-12-medium">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured"><img src="images/s30.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2 style="color: #76b900;">Tarjetas Gráficas RTX Series 30</h2>
												<p>La jugada maestra de Nvidia</p>
											</header>
											<p>Disfruta de los juegos con asombrosos niveles de detalle, altas resoluciones y rápidas frecuencias de cuadros.</p>
										</div>
									</section>

							</div>
							<div class="col-4 col-12-medium">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured"><img src="images/cod2.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2 style="color: #76b900;">Prueba los juegos con raytracing</h2>
												<p>Mejora tu experiencia del gaming</p>
											</header>
											<p>Inúndate en el mundo gamer con unos gráficos espectaculares y un trazado de rayos que te encantarán										                                        
											</p>
										</div>
									</section>

							</div>
							<div class="col-4 col-12-medium">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured"><img src="images/xd1.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2 style="color: #76b900;">Prueba nuestros nuevos dispositivos</h2>
												<p>Adquiere una potencia incomparable</p>
											</header>
											<p>Exprime el potencial de nuestros dispositivos que incluyen la nueva tecnología gráfica que tenemos para ti
											</p>
										</div>
									</section>

							</div>
						</div>
					</div>
				</div>

			<!-- Main -->
				<div id="main-wrapper">
					<div class="container">
						<div class="row gtr-200">
							<div class="col-4 col-12-medium">

								<!-- Sidebar -->
									<div id="sidebar">
										<section class="widget thumbnails">
											<h3>RayTracing en Juegos</h3>
											<div class="grid">
												<div class="row gtr-50">
													<div class="col-6"><a href="#" class="image fit"><img src="images/rtx1.jpg" alt="" /></a></div>
													<div class="col-6"><a href="#" class="image fit"><img src="images/rtx2.jpg" alt="" /></a></div>
													<div class="col-6"><a href="#" class="image fit"><img src="images/rtx3.jpg" alt="" /></a></div>
													<div class="col-6"><a href="#" class="image fit"><img src="images/rtx4.jpg" alt="" /></a></div>
												</div>
											</div>
										</section>
									</div>

							</div>
							<div class="col-8 col-12-medium imp-medium">

								<!-- Content -->
									<div id="content">
										<section class="last">
											<h2>¿Qué es el raytracing?</h2>
											<p>La tecnología RayTracing es el santo grial de las tarjetas gráficas para el mundo del gaming.</p>
											<p>Esta tecnología simula el comportamiento fisico de la luz y logra una renderización en tiempo real y de calidad cinematográfica hasta en los juegos mas exigentes</p>
											<a href="{{route('rtx')}}" class="button icon solid fa-arrow-circle-right" style="background-color: #76b900;">Seguir leyendo</a>
										</section>
									</div>

							</div>
						</div>
					</div>
				</div>
@endsection