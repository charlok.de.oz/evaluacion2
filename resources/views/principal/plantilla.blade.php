<!DOCTYPE HTML>
<!--
	Verti by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Nvidia Gforce</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="is-preload homepage" style="background-color: black;">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<header id="header" class="container">

						<!-- Logo -->
							<div id="logo">
								<a href="{{route('inicio')}}"><img src="images/iconortx.png" alt="" width="50%" height="50%"></a>
								<span>by Nvidia</span>
							</div>

						<!-- Nav -->
							<nav id="nav">
								<ul>
									<li class="current"><a href="{{route('inicio')}}">Inicio</a></li>
									<li><a href="{{route('rtx')}}">RTX ON</a></li>
									<li><a href="{{route('geforce')}}">Geforce Experience</a></li>
								</ul>
							</nav>

					</header>
				</div>

@yield('contenido')

			<!-- Footer -->
				<div id="footer-wrapper">
					<footer id="footer" class="container">
						<div class="row">
							<div class="col-3 col-6-medium col-12-small">

								<!-- Links -->
									<section class="widget links">
										<h3 style="color: white;">Productos</h3>
										<ul class="style2">
											<li><a href="#" style="color: #76b900;">Tarjetas Gráficas</a></li>
											<li><a href="#" style="color: #76b900;">Laptops</a></li>
											<li><a href="#" style="color:#76b900;">Monitores G-Sync</a></li>
											<li><a href="#" style="color: #76b900;">Gaming en la nube</a></li>
										</ul>
									</section>

							</div>
							<div class="col-3 col-6-medium col-12-small">

								<!-- Links -->
									<section class="widget links">
										<h3 style="color: white;">Comunidad y Noticias</h3>
										<ul class="style2">
											<li><a href="#" style="color: #76b900;">Noticias</a></li>
											<li><a href="#" style="color: #76b900;">Guias</a></li>
											<li><a href="#" style="color: #76b900;">Foro</a></li>
											<li><a href="#" style="color: #76b900;">E-Sports</a></li>
											<li><a href="#" style="color: #76b900;">Torneos</a></li>
										</ul>
									</section>

							</div>
							<div class="col-3 col-6-medium col-12-small">

								<!-- Links -->
									<section class="widget links">
										<h3 style="color: white;">Soporte</h3>
										<ul class="style2">
											<li><a href="#" style="color: #76b900;">Drivers</a></li>
											<li><a href="#" style="color: #76b900;">Soporte técnico</a></li>
											<li><a href="#" style="color: #76b900;">Atención a cliente</a></li>
											<li><a href="#" style="color: #76b900;">Deja tu queja aquí</a></li>
										</ul>
									</section>

							</div>
							<div class="col-3 col-6-medium col-12-small">

								<!-- Contact -->
									<section class="widget contact last">
										<h3 style="color: white;">Sigue a Gforce</h3>
										<ul>
											<li><a href="#" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
											<li><a href="#" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
											<li><a href="#" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
											<li><a href="#" class="icon brands fa-youtube"><span class="label">Youtube</span></a></li>
											<li><a href="#" class="icon brands fa-twitch"><span class="label">Twitch</span></a></li>
										</ul>
										<p>Siente el Poder<br />
										<br />
										</p>
									</section>

							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<div id="copyright">
									<ul class="menu">
										<li>&copy; Todos los derechos reservados</li><li> Diseño: <a href="https://www.facebook.com/charlok.rock.lml">CharlokDeOz</a></li>
									</ul>
								</div>
							</div>
						</div>
					</footer>
				</div>

			</div>

		<!-- Scripts -->

			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>