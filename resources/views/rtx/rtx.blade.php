@extends('principal.plantilla')

@section('contenido')
			<!-- Main -->
            <div id="main-wrapper">
					<div class="container">
						<div class="row gtr-200">
							<div class="col-4 col-12-medium">
								<div id="sidebar">

									<!-- Sidebar -->
										<section>
											<h3 style="color: #76b900;">Geforce Experience</h3>
											<p>Captura y comparte videos, capturas de pantalla y transmisiones en vivo con amigos. 
												Mantén tus drivers actualizados y optimiza la configuración de tu juego. 
												GeForce Experience™ te permite hacer todo, teniéndolo como el compañero esencial de tu tarjeta 
												gráfica GeForce®.</p>
											<footer>
												<a href="{{route('geforce')}}" class="button icon solid fa-info-circle" style="background-color: #76b900;">Conocer mas</a>
											</footer>
										</section>

										<section>
											<h3 style="color: #76b900;">Modelos de Tarjetas Gráficas</h3>
											<ul class="style2" >
												<li><a href="#">RTX 3090</a></li>
												<li><a href="#">RTX 3080</a></li>
												<li><a href="#">RTX 3070</a></li>
												<li><a href="#">RTX 2080 TI</a></li>
												<li><a href="#">RTX 2080</a></li>
												<li><a href="#">RTX 2070 Super</a></li>
											</ul>
										</section>

								</div>
							</div>
							<div class="col-8 col-12-medium imp-medium">
								<div id="content">

									<!-- Content -->
										<article>

											<h2 style="color: #76b900;">GEFORCE RTX Series 30</h2>

											<p>Las GPU GeForce RTX™ Serie 30 proporcionan el rendimiento definitivo
												 para los jugadores y los creadores. Cuentan con la tecnología Ampere, con la 
												 arquitectura de RTX de 2.ª generación de NVIDIA, con nuevos Núcleos RT, Núcleos 
												 Tensor y multiprocesadores de transmisión para potenciar los gráficos con ray tracing 
												 más realistas y las funciones de IA de vanguardia.</p>

											<h3 style="color: #ffffff;">Filtros de juego Freestyle</h3>
											<p>Los filtros de NVIDIA Freestyle te permiten añadir filtros de post-procesamiento
												 en tus juegos mientras juegas. Cambia la apariencia de tu juego con algunas variaciones 
												 de color o saturación, o aplica filtros de post-procesamiento de estilos como el HDR. 
												 Freestyle está integrado en el nivel del driver para una compatibilidad increíble con los 
												 juegos compatibles.</p>

											<p>NVIDIA DLSS es la revolucionaria renderización con IA que potencia las frecuencias de cuadros
												 con una calidad de imagen sin sacrificios gracias a los Núcleos Tensor de procesamiento de IA 
												 exclusivos en GPUs GeForce RTX. Esto te brinda la capacidad de rendimiento suficiente para llevar 
												 las opciones de configuración y los valores de resolución al máximo, lo que te permitirá disfrutar 
												 de una experiencia visual increíble.</p>

										</article>

								</div>
							</div>
						</div>
					</div>
				</div>
@endsection